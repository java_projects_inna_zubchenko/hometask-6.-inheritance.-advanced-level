package com.epam.test.automation.java.practice6;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.math.BigDecimal;

public class CompanyTest {
    @DataProvider(name = "nameMaxSalary")
    public Object[][] dataProvideMethod1(){
        return new Object[][] {{new Employee[]{new SalesPerson("Ivanov", BigDecimal.valueOf(1000), 115),
                new Manager( "Stepanchenko",  BigDecimal.valueOf(1100), 230),
                new Manager("Diachenko", BigDecimal.valueOf(1500), 550)}, new BigDecimal[]{new BigDecimal(100), new BigDecimal(200), new BigDecimal(300)}, "Diachenko"
        }};
    }
    @Test(dataProvider = "nameMaxSalary")
    public void testTask1(Employee[] employees, BigDecimal[] bonuses, String expected){

        for (int i = 0; i < employees.length; i++) {
            employees[i].setBonus(bonuses[i]);
        }
        Company company1 = new Company(employees);
        var actual = company1.nameMaxSalary();
        Assert.assertEquals(actual, expected, "The method nameMaxSalary returns incorrect value");
    }
}
