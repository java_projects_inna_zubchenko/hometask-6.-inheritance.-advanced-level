package com.epam.test.automation.java.practice6;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class SalesPersonTest {
    @DataProvider(name = "testToPay" )
    public Object[][] dataProviderMethod(){
        return new Object[][] {{new BigDecimal(130)}};
    }

    @Test(dataProvider = "testToPay")
    public void test(BigDecimal expected) {
        SalesPerson salesPerson1 = new SalesPerson("Ivanov", new BigDecimal(100), 90);
        salesPerson1.setBonus(new BigDecimal(30));
        var actual = salesPerson1.toPay();
        Assert.assertEquals(actual, expected, "The method toPay works incorrectly");
    }
}
