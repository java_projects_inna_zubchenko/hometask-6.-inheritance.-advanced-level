package com.epam.test.automation.java.practice6;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.math.BigDecimal;

public class ManagerTest {
    @DataProvider(name = "testToPay" )
    public Object[][] dataProviderMethod(){
        return new Object[][] {{new BigDecimal(1600)}};
    }

    @Test(dataProvider = "testToPay")
    public void test(BigDecimal expected) {
        Manager manager1 = new Manager("Ivanov", new BigDecimal(1000), 120);
        manager1.setBonus(new BigDecimal(100));
        var actual = manager1.toPay();
        Assert.assertEquals(actual, expected, "The method toPay works incorrectly");
    }
}
