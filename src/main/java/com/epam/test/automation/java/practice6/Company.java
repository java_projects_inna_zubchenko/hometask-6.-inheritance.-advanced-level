package com.epam.test.automation.java.practice6;

import java.math.BigDecimal;

public class Company {
    private final Employee[] employees;
    public Company(Employee[] employees)
    {
        this.employees = employees;
    }
    public void giveEverybodyBonus(BigDecimal companyBonus)
    {
        if(companyBonus == null||companyBonus.compareTo(BigDecimal.valueOf(0))<0){
            throw new IllegalArgumentException();
        }
        for (Employee employee: employees){
        employee.setBonus(companyBonus);
        }
    }
    public BigDecimal totalToPay(){
        BigDecimal sumTotal = new BigDecimal(0);
        for (Employee employee: employees){
            sumTotal = sumTotal.add(employee.toPay());
        }
        return sumTotal;
    }
    public String nameMaxSalary(){
        BigDecimal maxSalary = new BigDecimal(0);
        String lastName = null;
        for(Employee employee: employees){
            if(maxSalary.compareTo(employee.toPay())<0){
                maxSalary = employee.toPay();
                lastName = employee.getName();
            }
        }
        return lastName;
    }
}
