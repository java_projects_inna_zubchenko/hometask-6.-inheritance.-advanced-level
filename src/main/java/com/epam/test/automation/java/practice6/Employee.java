package com.epam.test.automation.java.practice6;

import java.math.BigDecimal;

public abstract class Employee {
    private final String name;
    private BigDecimal salary;
    protected BigDecimal bonus;

    protected Employee(String name, BigDecimal salary){
        this.name = name;
        this.salary = salary;
    }
    public String getName(){
        return name;
    }
    public void setSalary(BigDecimal salary){
        this.salary = salary;
    }
    public BigDecimal getSalary(){
        return salary;
    }
    public abstract void setBonus(BigDecimal bonus);
    public BigDecimal getBonus(){
        return bonus;
    }

    public BigDecimal toPay(){
        if(bonus == null||bonus.compareTo(BigDecimal.valueOf(0))<0){
            throw new IllegalArgumentException();
        }
        return bonus.add(salary);
    }
}
